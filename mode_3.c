#include "mcc_generated_files/mcc.h"
#include "main.h"
#include "mode_3.h"

int loop_mode_factorycheck(void)
{
	//動作モード
	int running_mode;
	//このループの動作モード
	const int this_mode = MY_RUNNING_MODE_FACTORYCHECK;
	running_mode = this_mode;
	//動作モードのLED表示
	MAIN_LedDisp(running_mode);
	
    while (running_mode == this_mode)
    {
        // Add your application code
		TEST_flushing();
		//SW1の状況を確認,動作モードの取得
		running_mode = MAIN_CheckRunningMode();
    }

	return running_mode;
}
