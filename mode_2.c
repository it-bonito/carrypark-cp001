#include "mcc_generated_files/mcc.h"
#include "main.h"
#include "mode_2.h"
#include "uart.h"


//RN4678 設定モード

enum {
	MODE2_CMD_RN_CMD = 1,
	MODE2_CMD_RN_AOK,
	MODE2_CMD_RN_REBOOT,
	MODE2_CMD_RN_REBOOTING,
};

const char RN_EnterCommoandMode[] = "$$$";
const char RN_SetFactoryParam[] = {'S','F',',','1',0x0d,0x00};
const char RN_SetModel[] = {'S','D','M',',','C','P','-','0','0','1',0x0d,0x00};
const char RN_SetMaker[] = {'S','D','N',',','C','a','r','r','y','P','a','r','k',0x0d,0x00};
const char RN_SetFwVersion[] = {'S','D','R',',','0','.','0','7',0x0d,0x00};
const char RN_SetDeviceName[] = {'S','-',',','C','P','0','0','1',0x0d,0x00};
const char RN_SetExtensionChar[] = {'S','O',',','<',',','>',0x0d,0x00};
const char RN_SetBluetoothMode[] = {'S','G',',','1',0x0d,0x00};
const char RN_Reset[] = {'R',',','1',0x0d,0x00};
const char RN_CMD_Return[] = {'C','M','D','>',' ',0x00};
const char RN_AOK_Return[] = {'A','O','K',0x0d,0x0a,'C','M','D','>',' ',0x00};
const char RN_Rebooting_Return[] = {'R','e','b','o','o','t','i','n','g',0x0d,0x0a,0x00};
const char RN_Reboot_Return[] = {'<','R','E','B','O','O','T','>',0x00};

typedef struct {
	const char *send_command;
	int expected_rtn;
} MODE1_SEND_RESULT;

MODE1_SEND_RESULT mode1_send_result[] = 
{
	//送信するコマンド           ,期待する返信結果
	{ RN_EnterCommoandMode,	MODE2_CMD_RN_CMD },
	{ RN_SetFactoryParam, 	MODE2_CMD_RN_AOK },
	{ RN_SetModel,          MODE2_CMD_RN_AOK },
	{ RN_SetMaker, 			MODE2_CMD_RN_AOK },
	{ RN_SetFwVersion, 		MODE2_CMD_RN_AOK },
	{ RN_SetDeviceName, 	MODE2_CMD_RN_AOK },
	{ RN_SetExtensionChar, 	MODE2_CMD_RN_AOK },
	{ RN_SetBluetoothMode, 	MODE2_CMD_RN_AOK },
};


CMD_CONFIG rn_receive_config[] =
{
	{ (const char *)RN_CMD_Return,	sizeof(RN_CMD_Return)-1,	MODE2_CMD_RN_CMD },
	{ (const char *)RN_AOK_Return,	sizeof(RN_AOK_Return)-1,	MODE2_CMD_RN_AOK },
	//{ (const char *)RN_Rebooting_Return,11,		MODE2_CMD_RN_REBOOTING },
	//{ (const char *)RN_Reboot_Return,	8,		MODE2_CMD_RN_REBOOT },
};
CMD_CONFIG rn_reboot_config[] =
{
	{ (const char *)RN_Rebooting_Return,sizeof(RN_Rebooting_Return)-1,	MODE2_CMD_RN_REBOOTING },
};

const char mode2_delim[] = {0x0a,0x00};

void SetRnParam(void){
	MODE1_SEND_RESULT *param = mode1_send_result;
	int param_len = sizeof(mode1_send_result)/sizeof(MODE1_SEND_RESULT);
	int i;
	int receive;
	UART_ClearRxCommand();
	for(i=0; i<param_len; ++i){
		UART_SendMessage(param->send_command, 0x00);
		//do{
		//	receive = UART_ReadRxCommand(rn_receive_config,sizeof(rn_receive_config)/sizeof(CMD_CONFIG)," ");
		//}while(receive == 0);
		//if(receive != param->expected_rtn){
		//	for(i=0; i<30; ++i){ IO_RB2_Toggle(); __delay_ms(100);}	//エラー表示
		//	break;
		//}
		param++;
		__delay_ms(1000);
		IO_RB2_Toggle();
	}
	//Reboot
	UART_SendMessage(RN_Reset, 0x00);
	__delay_ms(3000);
	//do{
	//	receive = UART_ReadRxCommand(rn_reboot_config, sizeof(rn_reboot_config)/sizeof(CMD_CONFIG),mode2_delim);
	//}//while(receive == 0);
	//if(receive != MODE2_CMD_RN_REBOOTING){
	//	for(i=0; i<30; ++i){ IO_RB2_Toggle(); __delay_ms(100);}	//エラー表示
	//	return;
	//}
	//終了表示
	for(i=0; i<5; ++i){
		IO_RB2_Toggle();
		__delay_ms(200);
	}
	IO_RB2_SetHigh();
}

int loop_mode_rnsetup(void)
{
	//動作モード
	int running_mode;
	//このループの動作モード
	const int this_mode = MY_RUNNING_MODE_RNSETUP;
	running_mode = this_mode;
	//動作モードのLED表示
	MAIN_LedDisp(running_mode);
    
    UART_ClearRxCommand();
	__delay_ms(500);
	
	SetRnParam();

	//動作モードのLED表示(再度）
	MAIN_LedDisp(running_mode);
	
    while (running_mode == this_mode)
    {
        // Add your application code
		//TEST_flushing();
		//SW1の状況を確認,動作モードの取得
		running_mode = MAIN_CheckRunningMode();
    }

	return running_mode;
}

