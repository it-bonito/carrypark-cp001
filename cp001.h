/* 
 * File:   cp100.h
 * Author: USER
 *
 * Created on 2020/01/13, 10:10
 */

#ifndef CP100_H
#define	CP100_H

#ifdef	__cplusplus
extern "C" {
#endif

enum {
	CP100_DONOT_MOVING = 0,
	CP100_CLOSING_1 = 101,
	CP100_CLOSING_2 = 102,
	CP100_CLOSE_FINISH = 103,
	CP100_CLOSE_ERR_NOTSTANBY = -101,
	CP100_CLOSE_ERR_TIMEOUT = -102,
	CP100_CLOSE_ERR_PLATE_DONOT_MOVE = -103,
	CP100_OPENING_1 = 201,
	CP100_OPENING_2 = 202,
	CP100_OPEN_FINISH = 203,
	CP100_OPEN_ERR_TIMEOUT = -202,
	CP100_OPEN_ERR_PLATE_DONOT_MOVE = -203,
};

#define CP100_LOCK_ERR_TIME (30*1000/10)
#define CP100_LOCKING_TIME (160/10)

#define CP100_SENS_OFF	(1)
#define CP100_SENS_ON	(0)

//フラグクリア
void CP100_FlagClear(void);
//クローズ動作
int CP100_CloseStart(void);
//オープン動作
int CP100_OpenStart(void);
//動作状況確認
int CP100_ReadRunningStatus(void);
//ステータス読み取り
unsigned char CP100_ReadStatus(void);
//バッテリー状態の読み取り
int CP100_ReadBattery(void);

//test
//ステータス受信
int CP100_Status(void);
//SetRn受信
int CP100_SetRn(void);
//内部エラー
//int CP100_Error(void);


#ifdef	__cplusplus
}
#endif

#endif	/* CP100_H */

