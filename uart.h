/* 
 * File:   uart.h
 * Author: USER
 *
 * Created on 2020/01/13, 10:17
 */

#ifndef UART_H
#define	UART_H

#ifdef	__cplusplus
extern "C" {
#endif

//コマンド解析構造体
typedef struct {
	const char *cmd;
	int			cmdlen;
	int			rtncmd;
} CMD_CONFIG;

enum {
	UART_RXCMD_OVERFLOW = -2,
};

//受信バッファを読み取り、そのまま返却する
//void UART_Echo(void);
//受信コマンド初期化
void UART_ClearRxCommand(void);
//受信バッファを読み取り、指定されたコマンドが来たか判断する
int UART_ReadRxCommand(CMD_CONFIG* cmd_config, int cmd_config_len, const char *delim);

//メッセージを送信する
void UART_SendMessage(const char *message, char delim);

#ifdef	__cplusplus
}
#endif

#endif	/* UART_H */

