/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.77
        Device            :  PIC18F24K22
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "mcc_generated_files/mcc.h"
#include "main.h"
#include "mode_1.h"
#include "mode_2.h"
#include "mode_3.h"


//グローバルメモリー
int GLOBAL_Tmr0_counter;

//ロック機構のタイマー
unsigned long GLOBAL_Lock_counter;
int GLOBAL_Lock_flag;

//SW1の長押しをコントロールする変数群
unsigned long GLOBAL_SW1_counter;
int GLOBAL_SW1_flag;

//SW1の長押しの検出と動作モードの検出
int MAIN_CheckRunningMode()
{
	static int my_runningMode = MY_RUNNING_MODE_NORMAL;

	//SW1の長押しを検出する
	if(IO_RC5_GetValue() == 0){						//SW1がONなら 10ms割り込みでカウントをアップさせる。
		GLOBAL_SW1_flag = 1;
	} else {
		if(GLOBAL_SW1_flag == 1){
			if(GLOBAL_SW1_counter >= 1000) {		//10秒以上おされていたらモードを工場出荷モードにする。
				my_runningMode = MY_RUNNING_MODE_FACTORYCHECK;
			} else if(GLOBAL_SW1_counter >= 500) {	//5秒以上押されていたらモードをRNセットアップモードにする。
				my_runningMode = MY_RUNNING_MODE_RNSETUP;
			} else {
				my_runningMode = MY_RUNNING_MODE_NORMAL;
			}
		}
		GLOBAL_SW1_flag = 0;
		GLOBAL_SW1_counter = 0;
	}
	return my_runningMode;
}

//動作モードのLEDによる表示
void MAIN_LedDisp(int _runningMode)
{
	switch(_runningMode){
		//動作モード: Normal mode
		case MY_RUNNING_MODE_NORMAL:
			IO_RB1_SetHigh();
			IO_RB2_SetLow();
			break;
		//動作モード: RN4678設定モード
		case MY_RUNNING_MODE_RNSETUP:
			IO_RB1_SetLow();
			IO_RB2_SetHigh();
			break;
		//動作モード: 工場出荷チェックモード
		case MY_RUNNING_MODE_FACTORYCHECK:
			IO_RB1_SetHigh();
			IO_RB2_SetHigh();
			break;
		default:
			break;
	}
}

//test
//LEDの点滅での動作確認
void TEST_flushing()
{
	if(GLOBAL_Tmr0_counter<=100) {
		IO_RA4_SetHigh();
	} else {
		IO_RA4_SetLow();
	}
	if(GLOBAL_Tmr0_counter>=200) GLOBAL_Tmr0_counter=0;
}

/*
                         Main application
 */
void main(void)
{
	int running_mode;
	
	//Boot Delay
	__delay_ms(100);
	
    // Initialize the device
    SYSTEM_Initialize();

    // If using interrupts in PIC18 High/Low Priority Mode you need to enable the Global High and Low Interrupts
    // If using interrupts in PIC Mid-Range Compatibility Mode you need to enable the Global and Peripheral Interrupts
    // Use the following macros to:

    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Enable the Peripheral Interrupts
    INTERRUPT_PeripheralInterruptEnable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();
	
	//動作モードを normal mode に設定
	running_mode = MY_RUNNING_MODE_NORMAL;
	
	//GLOBALメモリを初期化
	GLOBAL_Tmr0_counter = 0;
	GLOBAL_SW1_counter = 0;
	GLOBAL_SW1_flag = 0;
	
	//出力ポートの初期化
	IO_RB3_SetHigh();
	IO_RA4_SetLow();
	IO_RA5_SetLow();
	IO_RB1_SetLow();
	IO_RB2_SetLow();
	
	
	while(1){
		switch(running_mode)
		{
			case MY_RUNNING_MODE_NORMAL:
				running_mode = loop_mode_normal();
				break;
			case MY_RUNNING_MODE_RNSETUP:
				running_mode = loop_mode_rnsetup();
				break;
			case MY_RUNNING_MODE_FACTORYCHECK:
				running_mode = loop_mode_factorycheck();
				break;
			default:
				running_mode = MY_RUNNING_MODE_NORMAL;
				break;
		}
	}
}
/**
 End of File
*/