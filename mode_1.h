/* 
 * File:   mode_0.h
 * Author: USER
 *
 * Created on 2020/01/13, 9:51
 */

#ifndef MODE_0_H
#define	MODE_0_H

#ifdef	__cplusplus
extern "C" {
#endif

//モード: normal mode
int loop_mode_normal(void);

#ifdef	__cplusplus
}
#endif

#endif	/* MODE_0_H */

