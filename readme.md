# 本プロジェクトについて

本プロジェクトは CarryPark CP-001 用ファームウェアのプロジェクトです。  
CP-001基板に搭載された Microchip社製マイクロコントローラ PIC18F24K22 用のプログラムとその設計仕様書などを含むドキュメントを含みます。

# フォルダ構成

```
/               :本フォルダ全体が Microchip社のマイクロコントローラー PIC シーリーズの開発ツール MPLAB のプロジェクトフォルダーです。
+-- *.c         :CP-001 用ファームウェアのプログラムソース C コードです。
+-- *.h         :CP-001 用ファームウェアのプログラムヘッダファイルコードです。
+-- readme.mk   : このファイルです。
+-- Makefile    :CP-001 用ファームウェアのMakefile です。
+-- MyConfig.mc3:CP-001 用の PIC マイコンの設定ファイル(MPLAB用）です。
+--mcc_generate_files/
|               :MPLAB MCC が生成したPIC用の基本コード(c/hファイル)です。部分的に修正を入れています。
+--nbproject/
|               :MPLABの使用する設定ファイルなどが入っています。
+--documents/
   +-- readme.mk : このプロジェクトで生成される CP-001 用ファームウェアの使い方を書きます。
   +-- ble_interface.mk : CP-001基板と Bluetooth Low Energy で通信をする際のインターフェース仕様を書きます。
   +-- images/   : documnts フォルダ内の md ファイルから参照する画像データの置き場所です。
```

# 開発に必要なもの

本プロジェクトファイルは、ドキュメントファイルを除き、Microchip 社製 MPLAB 関連のプロジェクトファイルです。  
本プロジェクトファイルで生成される PIC18F24K22 用のプログラムをプリント基板上の PIC18 に書き込み動作させるためには以下のツールが必要です。

* MPLAB X IDE
 [Microchip社 MPLAB X IDE のサイト](https://www.microchip.com/mplab/mplab-x-ide)  
 お使いの環境に合わせてイントールしてください。インストール方法については、上記サイトを参照してください。

* MPLAB XC Compilers
 [Microchip社 MPLAB XC Compilers のサイト](https://www.microchip.com/mplab/compilers)  
 2019年12月現在、macOSの最新バージョン Catalina では動作しないので注意。  
> The latest version of macOS, 10.15 Catalina, removes support for 32-bit applications. This means that current versions of MPLAB XC32/32++ and MPLAB XC16 IDE will not run on this operating system. ...

* PICKit3
 [Microchip社 PICKit3のサイト](https://www.microchip.com/Developmenttools/ProductDetails/PG164130)  
 コンパイル・ビルドしたプログラムを本体基板に書き込むために必要です。  
 2020年1月現在 PIKkit4 も発売されているようですが、未確認状態です。

* iPhone と iPhone用Microchip社製アプリ SmartData
 CP-001 は Bluetooth Low Energy を使ってスマートフォンとやりとりをして動作しますので、スマートフォンと専用のアプリが必要です。  
 2020年1月現在、CarryPark 社提供アプリケーションはまだ開発されていませんので、iPhone と Microchip 社製アプリケーション SmartData が必要です。

# 使い方

MPLAB X IDE の使用方法に従って本プロジェクトをビルドし、PIKKit3 を利用して CP-001 の基板にプログラムを書き込んでください。

* 作成された CP-001 ファームウェアの動作方法については[こちら](documents/readme.md)を参照してください。
* Buetooth Low Energy を使用した CP-001 との通信インターフェースについては[こちら](documents/ble_interface.md)を参照してください。


