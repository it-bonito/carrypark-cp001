# 最初に

本ドキュメントは Markdown 形式で作成しています。  
このファイルは Web ブラウザで読んでください。  
[ブラウザでMarkdownを表示する方法](https://qiita.com/SUZUKI_Masaya/items/6476dbbcb3e369640c78)

# CarryPark 様向けプログラム

本プログラムは CarryPark 様むけ CP-001 用に書かれた、PIC18F24K22 用のプログラムです。  
2020年1月20日現在、回路図を元に作成したテストボードでの動作確認を行いましたが、試作基板での動作確認はできていません。
希望通り動かないケースもあると思いますので、ご注意ください。

## 動作概要

プログラムには、通常モードのモード１と Bluetooth チップ RN4678 への書き込みを行うモード２があります。  
モード３もテスト的に作成してありますが、2020年1月20日現在何も実装してありません。

### モード１：通常モード

電源を投入するとこのモードになります。スマートフォンなどで Bluetooth Low Energy を使用して接続し、コマンドを送信することで CP-001 を動作させることができます。  
このモードの時は 「キャリーパーク　CP-001B 3次試作 回路図」で示された D1 が点灯します。D2 は消灯です。

### モード２：RN4678 設定モード

上記モード１の状態から、SW1 を約8秒ほど押してから離すと、モード２になります。LED は D2 が点灯、D1 が消灯です。  
このモードでは予めPIC18F24K22に書き込まれた RN4678 の設定コマンドを順次発行し、RN4678 の設定を行います。  
一つの設定コマンドを出すごとに D2 の点灯状態が変わり、全ての書き込みが正しく終了すると数回フラッシュしてから、点灯状態に戻ります。  
モード1 に戻すには、一回 SW1 を押してください。

### モード３：

未実装です。


# プログラム書き込み方法

CarryPark 様との契約が締結できていないため、今回ソースコードは消した状態でのリリースとなります。  

1. ZIP ファイルを解凍する。
　添付した CP001v0_10.X.zip _ ファイルを解凍します。
2. nbproject > project.xml を MPLAB にドラッグ＆ドロップし、開きます。
3. 書き込みを行います。


# 動作確認方法

## LightBlue による RN4678 の確認

1. iPhone を準備してください。
1. App Store で LightBlue をダウンロードしてください。  
 ![LightBlueの説明画面](images/IMG_4050.PNG)
1. RN4678 の初期状態では RN4678-XXXX というデバイスが読み取れるはずです。
1. SW1 を7秒ほど長押しして、CP-001 基板の動作モードをモード2にします。D1 がオフ、D2 がオンです。
1. D2 が細かく点滅したら RN4678 の設定が終了します。
1. CP-001基板の電源を入れ直します。
1. 再度 LightBlue で BLE デバイスの検索を行います。  
 CP001-XXXX という文字が見えたら、RN4678 の設定は正常に終了しました。
1. CP001-XXXX に接続すると他の BLE のキャラクタリスティックは下の図のような設定になっているはずです。  
 ![設定状態](images/IMG_4051.PNG)  
 マニュファクチャが CarryPark に、モデルナンバーが CP-001 に、ソフトウェアリビジョンが 0.0x になっていることを確認してください。

## SmartData による CP-001 基板の動作確認

1. iPhone を準備してください。
1. App Store で SmartData をダウンロードしてください。  
 ![SmartDataの説明画面](images/IMG_4052.PNG)
1. SmartData を起動し、CP001-XXXX に接続します。
1. ステータスコマンドを発行してみます。  
 status;  
 status の後に、最後にセミコロン(;) をつけてください。そして「Send」をタップします。  
 本当はキャリッジリターン(CR, 0x0D)を送るべきなのですが、SmartData では入力できないため、セミコロン(;)を改行コードの代わりに利用しています。  
 ![status](images/IMG_4048.PNG)
1. 結果は以下のようになります。  
 ![status result](images/IMG_4049.PNG)
1.close; open: も同じように行います。  
 ![close](images/IMG_4046.PNG)
1. 結果のイメージ  
 ![close result](images/IMG_4047.PNG)


# Appendix

## LEDとSWの一覧

| SW1 | - | D1 | D2 |
|-----|---|----|----|
| 電源投入時 | モード1:通常モード | ON | OFF |
| 7秒程度長押し | モード2: RN4678設定 | OFF | ON |

## 施錠・解錠シーケンス

添付ファイル キャリーパーク施錠・解錠シーケンス.xlsx を確認してください。

## Status コマンドの読み方

| 記号 | 図面記号 | PIC 入力ピン | ON時の電圧レベル | Status表示 |
|-----|---------|-------------|---------------|------------|
|Neutral| アームニュートラルONセンサー | RC0 | Low | 1 |
|Standby| アームセットスタンバイONセンサー | RC1 | Low | 1 |
|Lock | アームロックONセンサー | RC2 | Low | 1 |
|JP1 | - | RC3 | Low | 1 |
|JP2 | - | RC4 | Low | 1 |
|SW1 | - | RC5 | Low | 1 |

## close/open エラー番号

| result | エラー内容 |
|--------|----------|
| -101 | 施錠(close)の初期状態エラー。施錠の開始時にアームスタンバイONセンサーがONのまま、またはアームニュートラルONセンサーがOFFのまま|
| -102 | 施錠(close)のタイムアウトエラー。モータを正転させたが30秒経過してもアームスタンバイONセンサーがONにならなかった。|
| -103 | 施業(close)の機器エラー。モータを正転させ、アームスタンバイONセンサーがONになったが、アームニュートラルONセンサーもONのまま。プレートが動いていない。|
| -202 | 解錠(open)のタイムアウトエラー。モータを正転させたが30秒経過してもアームスタンバイONセンサーがOFFにならなかった。|
| -203 | 解錠(open)の機器エラー。モータを正転させ、アームスタンバイONセンサーがOFFになったが、アームニュートラルONセンサーもOFFのまま。プレートが動いていない。|







