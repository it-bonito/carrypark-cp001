#include "mcc_generated_files/mcc.h"
#include "main.h"
#include "mode_1.h"
#include "uart.h"
#include "cp001.h"
#include <stdio.h>

#define BitCheck(a,b)	(a>>b)&0x01
enum {
	MODE0_CMD_OPEN = 1,
	MODE0_CMD_CLOSE,
	MODE0_CMD_STATUS,
	MODE0_CMD_SETRN,
};

const char open_str[] = {'o','p','e','n',0x0d,0x00};
const char close_str[] = {'c','l','o','s','e',0x0d,0x00};
const char status_str[] = {'s','t','a','t','u','s',0x0d,0x00};
const char setrn_str[] = {'s','e','t','r','n',0x0d,0x00};

CMD_CONFIG cmd_config[] =
{
	{ (const char *)open_str,	5,	MODE0_CMD_OPEN },
	{ (const char *)close_str,	6,	MODE0_CMD_CLOSE },
	{ (const char *)status_str,7,	MODE0_CMD_STATUS },
	{ (const char *)setrn_str,	6,	MODE0_CMD_SETRN },
	{ (const char *)"open;",	5,	MODE0_CMD_OPEN },
	{ (const char *)"close;",	6,	MODE0_CMD_CLOSE },
	{ (const char *)"status;",	7,	MODE0_CMD_STATUS },
	{ (const char *)"setrn;",	6,	MODE0_CMD_SETRN }
};

static void report(int command, int result)
{
	unsigned char pinSts;
	int batterySts;
	char message[96];
	pinSts = CP100_ReadStatus();
	batterySts = CP100_ReadBattery();
	switch(command){
		case MODE0_CMD_OPEN:
			switch(result){
				case CP100_OPEN_ERR_TIMEOUT:
					sprintf(message, "OPEN ERR=%d: TIMEOUT", result); 
					UART_SendMessage(message, 0x0d);
					break;
				case CP100_OPEN_ERR_PLATE_DONOT_MOVE:
					sprintf(message, "OPEN ERR=%d: Plate do not move", result); 
					UART_SendMessage(message, 0x0d);
					break;
				default:
					sprintf(message, "OPEN OK", result); 
					UART_SendMessage(message, 0x0d);
					break;
			}
			sprintf(message, "STATUS:Neutral=%d,Standby=%d,Lock=%d,JP1=%d,JP2=%d,SW1=%d,Battery=0x%04x",
						BitCheck(pinSts,0),BitCheck(pinSts,1),BitCheck(pinSts,2),
						BitCheck(pinSts,3),BitCheck(pinSts,4), BitCheck(pinSts,5),
						batterySts);
			UART_SendMessage(message, 0x0d);
			break;
		case MODE0_CMD_CLOSE:
			switch(result){
				case CP100_CLOSE_ERR_NOTSTANBY:
					sprintf(message, "CLOSE ERR=%d: NOT STANBY", result); 
					UART_SendMessage(message, 0x0d);
					break;
				case CP100_CLOSE_ERR_TIMEOUT:
					sprintf(message, "CLOSE ERR=%d: TIMEOUT", result); 
					UART_SendMessage(message, 0x0d);
					break;
				case CP100_CLOSE_ERR_PLATE_DONOT_MOVE:
					sprintf(message, "CLOSE ERR=%d: Plate do not move", result); 
					UART_SendMessage(message, 0x0d);
					break;
				default:
					sprintf(message, "CLOSE OK", result); 
					UART_SendMessage(message, 0x0d);
					break;
			}
			sprintf(message, "STATUS:Neutral=%d,Standby=%d,Lock=%d,JP1=%d,JP2=%d,SW1=%d,Battery=0x%04x",
						BitCheck(pinSts,0),BitCheck(pinSts,1),BitCheck(pinSts,2),
						BitCheck(pinSts,3),BitCheck(pinSts,4), BitCheck(pinSts,5),
						batterySts);
			UART_SendMessage(message, 0x0d);
			break;
		case MODE0_CMD_STATUS:
			sprintf(message, "STATUS:Neutral=%d,Standby=%d,Lock=%d,JP1=%d,JP2=%d,SW1=%d,Battery=0x%04x",
						BitCheck(pinSts,0),BitCheck(pinSts,1),BitCheck(pinSts,2),
						BitCheck(pinSts,3),BitCheck(pinSts,4), BitCheck(pinSts,5),
						batterySts);
			UART_SendMessage(message, 0x0d);
			break;
		case MODE0_CMD_SETRN:
			sprintf(message, "SETRN STATUS:Neutral=%d,Standby=%d,Lock=%d,JP1=%d,JP2=%d,SW1=%d,Battery=0x%04x",
						BitCheck(pinSts,0),BitCheck(pinSts,1),BitCheck(pinSts,2),
						BitCheck(pinSts,3),BitCheck(pinSts,4), BitCheck(pinSts,5),
						batterySts);
			UART_SendMessage(message, 0x0d);
			break;
		default:
			break;
	}
}

const char mode1_delim[] = {';','>',0x0d,0x00};

//モード: normal mode
int loop_mode_normal(void)
{
	//実動作の結果
	int result;
	//受信コマンドの読み取り
	int rxcmd;
	//動作モード
	int running_mode;
	//このループの動作モード
	const int this_mode = MY_RUNNING_MODE_NORMAL;
	running_mode = this_mode;
	//動作モードのLED表示
	MAIN_LedDisp(running_mode);
	//送信クリア
	UART_SendMessage("clear", 0x0d);
	//受信コマンドの初期化
	UART_ClearRxCommand();
	
    while (running_mode == this_mode)
    {
        // Add your application code
		//受信コマンドの読み取り
		rxcmd = UART_ReadRxCommand(cmd_config, sizeof(cmd_config)/sizeof(CMD_CONFIG),(const char*)mode1_delim);
		switch(rxcmd){
			case MODE0_CMD_OPEN:
				result = CP100_OpenStart();
				if(result != 0){
					report(MODE0_CMD_OPEN, result);
				}
				break;
			case MODE0_CMD_CLOSE:
				result = CP100_CloseStart();
				if(result != 0){
					report(MODE0_CMD_CLOSE, result);
				}
				break;
			case MODE0_CMD_STATUS:
				result = CP100_Status();
				report(MODE0_CMD_STATUS, result);
				break;
			case MODE0_CMD_SETRN:
				result = CP100_SetRn();
				report(MODE0_CMD_SETRN, result);
				break;
				
			//エラー系
			case UART_RXCMD_OVERFLOW:
				break;
			default:
				break;
		}
		//実動作の読み取り
		result = CP100_ReadRunningStatus();
		switch(result){
			case CP100_CLOSE_FINISH:
				report(MODE0_CMD_CLOSE, 0);
				CP100_FlagClear();
				break;
			case CP100_OPEN_FINISH:
				report(MODE0_CMD_OPEN, 0);
				CP100_FlagClear();
				break;
			case CP100_CLOSE_ERR_TIMEOUT:
				report(MODE0_CMD_CLOSE, CP100_CLOSE_ERR_TIMEOUT);
				CP100_FlagClear();
				break;
			case CP100_OPEN_ERR_TIMEOUT:
				report(MODE0_CMD_OPEN, CP100_OPEN_ERR_TIMEOUT);
				CP100_FlagClear();
				break;
			case CP100_CLOSE_ERR_PLATE_DONOT_MOVE:
				report(MODE0_CMD_CLOSE, CP100_CLOSE_ERR_PLATE_DONOT_MOVE);
				CP100_FlagClear();
				break;
			case CP100_OPEN_ERR_PLATE_DONOT_MOVE:
				report(MODE0_CMD_OPEN, CP100_OPEN_ERR_PLATE_DONOT_MOVE);
				CP100_FlagClear();
				break;
			default:
				break;
		}
		//SW1の状況を確認,動作モードの取得
		running_mode = MAIN_CheckRunningMode();
		//動作中はモードの切り替えを無効にする。
		if(result != CP100_DONOT_MOVING){
			running_mode = this_mode;
		}
    }

	return running_mode;
}

