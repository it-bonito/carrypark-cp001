/* 
 * File:   main.h
 * Author: USER
 *
 * Created on 2020/01/13, 9:58
 */

#ifndef MAIN_H
#define	MAIN_H

#ifdef	__cplusplus
extern "C" {
#endif

//動作モードの定義
enum {
	MY_RUNNING_MODE_NORMAL = 1,
	MY_RUNNING_MODE_RNSETUP,
	MY_RUNNING_MODE_FACTORYCHECK,
};

//SW1の長押しの検出と動作モードの検出
int MAIN_CheckRunningMode(void);
//動作モードのLEDによる表示
void MAIN_LedDisp(int _runningMode);
//test
//LEDの点滅での動作確認
void TEST_flushing(void);


#ifdef	__cplusplus
}
#endif

#endif	/* MAIN_H */

