#include "mcc_generated_files/mcc.h"
#include "uart.h"
#include <string.h>

#define RXCMD_BUFFER_LEN (32)

unsigned char rx_commnad[RXCMD_BUFFER_LEN];
int rx_command_len;

//受信バッファを読み取り、そのまま返却する
/*
void UART_Echo()
{
	volatile uint8_t rxData;
	if(EUSART1_is_rx_ready())
    {
        rxData = EUSART1_Read();
        if(EUSART1_is_tx_ready())
        {
            EUSART1_Write(rxData);
        }
    }
}
*/

//受信コマンド初期化
void UART_ClearRxCommand(void)
{
	int i;
	rx_command_len = 0;
	for(i=0; i<RXCMD_BUFFER_LEN; ++i){ rx_commnad[i]=0x00; }
}

//受信コマンドの読み取り
int UART_ReadRxCommand(CMD_CONFIG* cmd_config, int cmd_config_len, const char *delim)
{
	volatile uint8_t rxData;
	if(EUSART1_is_rx_ready())
    {
		int i;
        rxData = EUSART1_Read();
		rx_commnad[rx_command_len++] = rxData;
		if(rx_command_len >= RXCMD_BUFFER_LEN){
			UART_ClearRxCommand();
			return UART_RXCMD_OVERFLOW;
		}
		//if(rxData == ';' || rxData == 0x0d || rxData == '>'){
		if(strchr(delim, (int)rxData) != (char*)0){
			for(i=0; i<cmd_config_len; ++i){
				if(cmd_config->cmdlen == rx_command_len){
					if(strncmp((char*)cmd_config->cmd, (const char*)rx_commnad, rx_command_len) == 0){
						UART_ClearRxCommand();
						return cmd_config->rtncmd;
					}
				}
                cmd_config++;
			}
			UART_ClearRxCommand();
		}
    }
	return 0;
}

//メッセージを送信する
void UART_SendMessage(const char *message, char delim)
{
	volatile uint8_t txData;
	while((txData = (uint8_t)(*message++)) != 0x00){
        while(EUSART1_is_tx_ready() == 0){
		}
        EUSART1_Write(txData);
	}
	if(delim){
        while(EUSART1_is_tx_ready() == 0){
		}
        EUSART1_Write(delim);
	}
}
		
