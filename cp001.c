#include "mcc_generated_files/mcc.h"
#include "cp001.h"

extern unsigned long GLOBAL_Lock_counter;
extern int GLOBAL_Lock_flag;

//動作中フラグ
static int close_running;
static int open_running;

//フラグクリア
void CP100_FlagClear(void)
{
	close_running = CP100_DONOT_MOVING;
	open_running = CP100_DONOT_MOVING;
}

//クローズ動作
int CP100_CloseStart(void)
{
	//アームセットスタンバイONセンサーがオフである事を確認
	if(IO_RC1_GetValue()==CP100_SENS_ON){
		return CP100_CLOSE_ERR_NOTSTANBY;
	}
	//アームニュートラルONセンサーがONであることを確認
	if(IO_RC0_GetValue()==CP100_SENS_OFF){
		return CP100_CLOSE_ERR_NOTSTANBY;
	}
	//アラームタイマーをスタート
	GLOBAL_Lock_counter = 0;
	GLOBAL_Lock_flag = 1;
	//モーターを正転させる
	IO_RA4_SetHigh();
	IO_RA5_SetLow();
	//動作中フラグをセット
	close_running = CP100_CLOSING_1;
	open_running = CP100_DONOT_MOVING;
	return 0;
}

//オープン動作
int CP100_OpenStart(void)
{
	//アラームタイマーをスタート
	GLOBAL_Lock_counter = 0;
	GLOBAL_Lock_flag = 1;
	//モーターを正転させる
	IO_RA4_SetHigh();
	IO_RA5_SetLow();
	//動作中フラグをセット
	open_running = CP100_OPENING_1;
	close_running = CP100_DONOT_MOVING;
	return 0;
}

//動作状況確認
int CP100_ReadRunningStatus(void)
{
	//施錠動作
	if(close_running != CP100_DONOT_MOVING){
		switch(close_running)
		{
			case CP100_CLOSING_1:
				//アームセットスタンバイONセンサーがONである事を確認
				if(IO_RC1_GetValue()==CP100_SENS_OFF)	//センサーはOFF
				{
					if(GLOBAL_Lock_counter >= CP100_LOCK_ERR_TIME)
					{
						//モーターフリー
						IO_RA4_SetLow();
						IO_RA5_SetLow();
						//アラームタイマーをストップ
						GLOBAL_Lock_counter = 0;
						GLOBAL_Lock_flag = 0;
						//動作フラグを停止
						close_running = CP100_DONOT_MOVING;
						//エラーを返却
						return CP100_CLOSE_ERR_TIMEOUT;
					}
					close_running = CP100_CLOSING_1;
				} else {				//センサーがON
					//アラームタイマーをストップ
					GLOBAL_Lock_counter = 0;
					GLOBAL_Lock_flag = 0;
					//アームニュートラルONセンサーを確認
					if(IO_RC0_GetValue()==CP100_SENS_ON){
						//モーターフリー
						IO_RA4_SetLow();
						IO_RA5_SetLow();
						//動作フラグを停止
						close_running = CP100_DONOT_MOVING;
						//モーターが回ったのに金属板が動いていないエラー
						return CP100_CLOSE_ERR_PLATE_DONOT_MOVE;
					}
					//モーターフリー
					IO_RA4_SetLow();
					IO_RA5_SetLow();
					//動作フラグを停止
					close_running = CP100_CLOSE_FINISH;
				}
				break;
			default:
				//モーターフリー
				IO_RA4_SetLow();
				IO_RA5_SetLow();
				//アラームタイマーをストップ
				GLOBAL_Lock_counter = 0;
				GLOBAL_Lock_flag = 0;
				//動作フラグを停止
				close_running = CP100_DONOT_MOVING;
		}
		return close_running;
	}
	//開錠動作
	else if(open_running != CP100_DONOT_MOVING){
		switch(open_running)
		{
			case CP100_OPENING_1:
				//アームセットスタンバイONセンサーがOFFである事を確認
				if(IO_RC1_GetValue()==CP100_SENS_ON)	//センサーはON
				{
					if(GLOBAL_Lock_counter >= CP100_LOCK_ERR_TIME)
					{
						//モーターフリー
						IO_RA4_SetLow();
						IO_RA5_SetLow();
						//アラームタイマーをストップ
						GLOBAL_Lock_counter = 0;
						GLOBAL_Lock_flag = 0;
						//動作フラグを停止
						open_running = CP100_DONOT_MOVING;
						//エラーを返却
						return CP100_OPEN_ERR_TIMEOUT;
					}
					open_running = CP100_OPENING_1;
				} else {				//センサーがOFF
					//アラームタイマーをストップ
					GLOBAL_Lock_counter = 0;
					GLOBAL_Lock_flag = 0;
					//アームニュートラルONセンサーを確認
					if(IO_RC0_GetValue()==CP100_SENS_OFF){
						//モーターフリー
						IO_RA4_SetLow();
						IO_RA5_SetLow();
						//動作フラグを停止
						open_running = CP100_DONOT_MOVING;
						//モーターが回ったのに金属板が動いていないエラー
						return CP100_OPEN_ERR_PLATE_DONOT_MOVE;
					}
					//モーターフリー
					IO_RA4_SetLow();
					IO_RA5_SetLow();
					//動作フラグを停止
					open_running = CP100_OPEN_FINISH;
				}
				break;
			default:
				//モーターフリー
				IO_RA4_SetLow();
				IO_RA5_SetLow();
				//アラームタイマーをストップ
				GLOBAL_Lock_counter = 0;
				GLOBAL_Lock_flag = 0;
				//動作フラグを停止
				open_running = CP100_DONOT_MOVING;
		}
		return open_running;
	}
	return CP100_DONOT_MOVING;
}





//ステータス読み取り
unsigned char CP100_ReadStatus(void)
{
	return ~PORTC;
}

//バッテリー状態の読み取り
int CP100_ReadBattery(void)
{
    uint16_t convertedValue;

    ADC_Initialize();
	ADC_SelectChannel(channel_AN2);
    ADC_StartConversion();

    while(!ADC_IsConversionDone());
    convertedValue = ADC_GetConversionResult();
	return (int)(convertedValue&0xffff);
}

//test
//ステータス受信
int CP100_Status(void)
{
	IO_RA4_SetHigh();
	IO_RA5_SetHigh();
	__delay_ms(300);
	IO_RA4_SetLow();
	IO_RA5_SetLow();
	__delay_ms(300);
	IO_RA4_SetHigh();
	IO_RA5_SetHigh();
	__delay_ms(300);
	IO_RA4_SetLow();
	IO_RA5_SetLow();
	return 0;
}
	
//SetRn受信
int CP100_SetRn(void)
{
	IO_RA4_SetHigh();
	IO_RA5_SetLow();
	__delay_ms(300);
	IO_RA4_SetLow();
	IO_RA5_SetHigh();
	__delay_ms(300);
	IO_RA4_SetHigh();
	IO_RA5_SetLow();
	__delay_ms(300);
	IO_RA4_SetLow();
	IO_RA5_SetHigh();
	__delay_ms(300);
	IO_RA4_SetLow();
	IO_RA5_SetLow();
	return 0;
}

//内部エラー
/*
int CP100_Error(void)
{
	while(1)
	{
		IO_RB1_SetHigh();
		IO_RB2_SetLow();
		__delay_ms(1000);
		IO_RB1_SetLow();
		IO_RB2_SetHigh();
		__delay_ms(1000);
	}
	return 0;
}
*/
